# vim:set ts=4 sw=4 et nowrap syntax=python ff=unix:
#
# Copyright 2020 Mark Crewson <mark@crewson.net>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest

from packmaker import packdef, packlock, resolver
from packmaker.framew.config import Config
from packmaker.framew.application import OperationError

from .mockapi import MockApi

##############################################################################


class TestResolver (object):

    JEI_VERSION_ALPHA = "jei-1.18.2-forge-10.0.0.191.jar"
    JEI_VERSION_BETA = "jei-1.18.2-9.7.0.180.jar"
    JEI_VERSION_RELEASE = "jei-1.18.2-9.5.5.174.jar"

    JEI_VERSION_LATEST = JEI_VERSION_ALPHA

    def make_resolver(self, pack):
        r = resolver.Resolver(MockConfig(), pack)
        r.api = MockApi()
        return r

    def test_resolve_simple_mod(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('jei', None), 'mod', lock)
        assert len(lock.get_all_mods()) == 1
        assert len(lock.get_all_resourcepacks()) == 0

        mod = lock.get_mod('jei')
        assert mod.version == self.JEI_VERSION_LATEST

    def test_resolve_missing_mod(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        with pytest.raises(OperationError):
            r.resolve_addon(packdef.ModDefinition('nosuchmod', None), 'mod', lock)

    def test_resolve_mod_by_curseforge_id(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('tinkers-construct', {'curseforgeid': '74924'}), 'mod', lock)
        assert len(lock.get_all_mods()) == 1
        assert lock.get_mod('tinkers-construct')

    def test_resolve_bad_curseforge_id(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        with pytest.raises(OperationError):
            r.resolve_addon(packdef.ModDefinition('tinkers-construct', {'curseforgeid': 'aaaaaaa'}), 'mod', lock)

    def test_resolve_mod_by_raw_url(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('raw-url-mod', {'url': 'https://www.somesite.com/path/to/url/mod.jar'}),
                        'mod', lock)
        assert len(lock.get_all_mods()) == 1
        assert lock.get_mod('raw-url-mod')

    def test_resolve_mod_release(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        release = 'jei-1.18.2-9.5.4.171.jar'
        r.resolve_addon(packdef.ModDefinition('jei', {'release': release}), 'mod', lock)
        assert len(lock.get_all_mods()) == 1

        mod = lock.get_mod('jei')
        assert mod.version == release

    def test_resolve_mod_release_releasetype_release(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        release = 'latest-release'
        r.resolve_addon(packdef.ModDefinition('jei', {'release': release}), 'mod', lock)
        assert len(lock.get_all_mods()) == 1

        mod = lock.get_mod('jei')
        assert mod.version == self.JEI_VERSION_RELEASE

    def test_resolve_mod_release_releasetype_beta(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        release = 'latest-beta'
        r.resolve_addon(packdef.ModDefinition('jei', {'release': release}), 'mod', lock)
        assert len(lock.get_all_mods()) == 1

        mod = lock.get_mod('jei')
        assert mod.version == self.JEI_VERSION_BETA

    def test_resolve_mod_release_releasetype_alpha(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        release = 'latest-alpha'
        r.resolve_addon(packdef.ModDefinition('jei', {'release': release}), 'mod', lock)
        assert len(lock.get_all_mods()) == 1

        mod = lock.get_mod('jei')
        assert mod.version == self.JEI_VERSION_ALPHA

    def test_resolve_mod_release_pack_releasetype_release(self):
        pack = MockPackDefinition()
        pack.set_mod_releasetypes(['release'])
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('jei', None), 'mod', lock)
        assert len(lock.get_all_mods()) == 1

        mod = lock.get_mod('jei')
        assert mod.version == self.JEI_VERSION_RELEASE

    def test_resolve_mod_release_pack_releasetype_beta(self):
        pack = MockPackDefinition()
        pack.set_mod_releasetypes(['beta'])
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('jei', None), 'mod', lock)
        assert len(lock.get_all_mods()) == 1

        mod = lock.get_mod('jei')
        assert mod.version == self.JEI_VERSION_BETA

    def test_resolve_mod_release_pack_releasetype_beta2(self):
        pack = MockPackDefinition()
        pack.set_mod_releasetypes(['beta', 'release'])
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('jei', None), 'mod', lock)
        assert len(lock.get_all_mods()) == 1

        mod = lock.get_mod('jei')
        assert mod.version == self.JEI_VERSION_BETA

    def test_resolve_mod_release_pack_releasetype_alpha(self):
        pack = MockPackDefinition()
        pack.set_mod_releasetypes(['alpha'])
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('jei', None), 'mod', lock)
        assert len(lock.get_all_mods()) == 1

        mod = lock.get_mod('jei')
        assert mod.version == self.JEI_VERSION_ALPHA

    def test_resolve_mod_release_pack_releasetype_alpha2(self):
        pack = MockPackDefinition()
        pack.set_mod_releasetypes(['alpha', 'beta'])
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('jei', None), 'mod', lock)
        assert len(lock.get_all_mods()) == 1

        mod = lock.get_mod('jei')
        assert mod.version == self.JEI_VERSION_ALPHA

    def test_resolve_mod_release_pack_releasetype_alpha3(self):
        pack = MockPackDefinition()
        pack.set_mod_releasetypes(['alpha', 'release'])
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('jei', None), 'mod', lock)
        assert len(lock.get_all_mods()) == 1

        mod = lock.get_mod('jei')
        assert mod.version == self.JEI_VERSION_ALPHA

    def test_resolve_mod_with_dependencies(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('tinkers-construct', None), 'mod', lock)
        assert len(lock.get_all_mods()) == 2
        assert lock.get_mod('tinkers-construct')
        assert lock.get_mod('mantle')

    def test_resolve_mod_with_dependencies_already_resolved(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('mantle', None), 'mod', lock)
        r.resolve_addon(packdef.ModDefinition('tinkers-construct', None), 'mod', lock)
        assert len(lock.get_all_mods()) == 2
        assert lock.get_mod('tinkers-construct')
        assert lock.get_mod('mantle')

    def test_resolve_mod_with_dependencies_in_packdef(self):
        pack = MockPackDefinition()
        pack.add_mod('mantle', None)
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('tinkers-construct', None), 'mod', lock)

        # only 1 mod resolved here as we're not actually resolving 'mantle'
        # normally in this situation, 'mantle' would be resolved as part of
        # one of the mods defined in the packdef...
        assert len(lock.get_all_mods()) == 1
        assert lock.get_mod('tinkers-construct')

    def test_resolve_mod_with_ignore_all_deps(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('tinkers-construct', {'ignoreddeps': 'all'}), 'mod', lock)
        assert len(lock.get_all_mods()) == 1

    def test_resolve_mod_with_ignore_dep(self):
        pack = MockPackDefinition()
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ModDefinition('tinkers-construct', {'ignoreddeps': ['mantle']}), 'mod', lock)
        assert len(lock.get_all_mods()) == 1

    def test_resolve_simple_resourcepack(self):
        pack = MockPackDefinition(minecraft_version='1.12.2', forge_version='14.23.5.2860')
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        r.resolve_addon(packdef.ResourcepackDefinition('john-smith-legacy-modded', None), 'resourcepack', lock)
        assert len(lock.get_all_mods()) == 0
        assert len(lock.get_all_resourcepacks()) == 1

    def test_resolve_missing_resourcepack(self):
        pack = MockPackDefinition(minecraft_version='1.12.2', forge_version='14.23.5.2860')
        lock = packlock.PackLock(None)
        r = self.make_resolver(pack)

        with pytest.raises(OperationError):
            r.resolve_addon(packdef.ResourcepackDefinition('nosuchrp', None), 'resourcepack', lock)

##############################################################################


class MockConfig (Config):
    def get(self, param):
        if param == 'curseforge::authentication_token':
            return 'mock-token'
        return None


class MockPackDefinition (packdef.PackDefinition):

    def __init__(self, minecraft_version='1.18.2', forge_version='40.1.0'):
        super(MockPackDefinition, self).__init__([])
        self.minecraft_version = minecraft_version
        self.forge_version = forge_version

    def add_mod(self, slug, moddef):
        self.mods[slug] = packdef.ModDefinition(slug, moddef)

    def add_resourcepack(self, slug, respdef):
        self.resourcepacks[slug] = packdef.ResourcepackDefinition(slug, respdef)

    def set_mod_releasetypes(self, releasetypes):
        self.mod_releasetypes = releasetypes

    def set_resourcepack_releasetypes(self, releasetypes):
        self.resourcepack_releasetypes = releasetypes

##############################################################################
# THE END
