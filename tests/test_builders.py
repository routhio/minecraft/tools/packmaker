# vim:set ts=4 sw=4 et nowrap syntax=python ff=unix:
#
# Copyright 2020 Mark Crewson <mark@crewson.net>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

from packmaker.builder.curseforge import CurseforgeBuilder
# from packmaker.builder.local import LocalBuilder
from packmaker.builder.pack import PackBuilder
from packmaker.builder.routhio import RouthioBuilder
from packmaker.builder.server import ServerBuilder

##############################################################################


class Base_TestBuilder (object):

    builderclass = None

    def run_build(self, packlock_string, tmp_path):
        class Args(object):
            def __init__(self):
                self.build_dir = os.path.join(tmp_path, 'build')
                self.cache_dir = os.path.join(tmp_path, 'cache')
                self.release_dir = os.path.join(tmp_path, 'release')
                self.lockfile = None
                self.release_format = 'zip'

        args = Args()
        builder = self.builderclass({})

        packlock_filename = os.path.join(tmp_path, 'packmaker.lock')
        with open(packlock_filename, 'wt') as f:
            f.write(packlock_string)
            f.flush()
        args.lockfile = [f.name]
        builder.setup_build(args)
        builder.do_build()
        return builder


class TestBuilder_Curseforge(Base_TestBuilder):
    builderclass = CurseforgeBuilder

    def test_forge_build(self, tmp_path):
        self.run_build(forge_packlock, tmp_path)
        releasefile = os.path.join(tmp_path, 'release', 'examplepack-versionX.zip')
        assert os.path.exists(releasefile)

    def test_fabric_build(self, tmp_path):
        self.run_build(fabric_packlock, tmp_path)
        releasefile = os.path.join(tmp_path, 'release', 'examplepack-versionX.zip')
        assert os.path.exists(releasefile)


# class TestBuilder_Local(Base_TestBuilder):
#     builderclass = LocalBuilder
#     released_file = None


class TestBuilder_Pack(Base_TestBuilder):
    builderclass = PackBuilder

    def test_forge_build(self, tmp_path):
        self.run_build(forge_packlock, tmp_path)
        releasefile = os.path.join(tmp_path, 'release', 'examplepack-versionX.pack')
        assert os.path.exists(releasefile)

    def test_fabric_build(self, tmp_path):
        self.run_build(fabric_packlock, tmp_path)
        releasefile = os.path.join(tmp_path, 'release', 'examplepack-versionX.pack')
        assert os.path.exists(releasefile)


class TestBuilder_Routhio(Base_TestBuilder):
    builderclass = RouthioBuilder

    def test_forge_build(self, tmp_path):
        self.run_build(forge_packlock, tmp_path)
        releasefile = os.path.join(tmp_path, 'release', 'examplepack-versionX-routhio.zip')
        assert os.path.exists(releasefile)

    def test_fabric_build(self, tmp_path):
        self.run_build(fabric_packlock, tmp_path)
        releasefile = os.path.join(tmp_path, 'release', 'examplepack-versionX-routhio.zip')
        assert os.path.exists(releasefile)


class TestBuilder_Server(Base_TestBuilder):
    builderclass = ServerBuilder

    def test_forge_build(self, tmp_path):
        self.run_build(forge_packlock, tmp_path)
        assert os.path.exists(os.path.join(tmp_path, 'build', 'server', 'forge_server-1.12.2-14.23.5.2854.jar'))
        assert os.path.exists(os.path.join(tmp_path, 'build', 'server', 'minecraft_server.1.12.2.jar'))

#    def test_fabric_build(self, tmp_path):
#        self.run_build(fabric_packlock, tmp_path)
#        assert os.path.exists(os.path.join(tmp_path, 'build', 'server', 'fabric-server-launch.jar'))
#        assert os.path.exists(os.path.join(tmp_path, 'build', 'server', 'minecraft_server.1.12.2.jar'))

##############################################################################


forge_packlock = """
{
   "extraopt" : {},
   "files" : [],
   "metadata" : {
      "authors" : [
         "mcrewson"
      ],
      "forge_version" : "14.23.5.2854",
      "minecraft_version" : "1.12.2",
      "name" : "examplepack",
      "title" : "Example Pack",
      "version" : "versionX"
   },
   "resolutions" : {
      "john-smith-legacy-modded" : {
         "author" : "xMrVizzy",
         "clientonly" : null,
         "description" : null,
         "downloadUrl" : "https://edge.forgecdn.net/files/2480/942/Faithful 1.12.2-rv4.zip",
         "fileId" : 2480942,
         "fileName" : "Faithful 1.12.2-rv4.zip",
         "name" : "Faithful x32",
         "optional" : false,
         "projectId" : 236821,
         "recommendation" : "starred",
         "selected" : false,
         "serveronly" : null,
         "type" : "resourcepack",
         "version" : "latest",
         "website" : "https://www.curseforge.com/minecraft/texture-packs/john-smith-legacy-modded"
      },
      "jei" : {
         "author" : "mezz",
         "clientonly" : false,
         "description" : null,
         "downloadUrl" : "https://edge.forgecdn.net/files/2847/112/jei_1.12.2-4.15.0.293.jar",
         "fileId" : 2847112,
         "fileName" : "jei_1.12.2-4.15.0.293.jar",
         "name" : "Just Enough Items (JEI)",
         "optional" : false,
         "projectId" : 238222,
         "recommendation" : "starred",
         "selected" : false,
         "serveronly" : false,
         "type" : "mod",
         "version" : "latest",
         "website" : "https://www.curseforge.com/minecraft/mc-mods/jei"
      }
   },
   "version" : "1"
}
"""

fabric_packlock = """
{
   "extraopt" : {},
   "files" : [],
   "metadata" : {
      "authors" : [
         "mcrewson"
      ],
      "fabric_version" : "0.10.8",
      "minecraft_version" : "1.16.4",
      "name" : "examplepack",
      "title" : "Example Pack",
      "version" : "versionX"
   },
   "resolutions" : {
      "john-smith-legacy-modded" : {
         "author" : "xMrVizzy",
         "clientonly" : null,
         "description" : null,
         "downloadUrl" : "https://edge.forgecdn.net/files/2480/942/Faithful 1.12.2-rv4.zip",
         "fileId" : 2480942,
         "fileName" : "Faithful 1.12.2-rv4.zip",
         "name" : "Faithful x32",
         "optional" : false,
         "projectId" : 236821,
         "recommendation" : "starred",
         "selected" : false,
         "serveronly" : null,
         "type" : "resourcepack",
         "version" : "latest",
         "website" : "https://www.curseforge.com/minecraft/texture-packs/john-smith-legacy-modded"
      },
      "fabric-api" : {
         "author" : "asiekierka",
         "clientonly" : false,
         "description" : null,
         "downloadUrl" : "https://edge.forgecdn.net/files/3141/112/fabric-api-0.28.3+1.16.jar",
         "fileId" : 3141112,
         "fileName" : "fabric-api-0.28.3+1.16.jar",
         "name" : "Fabric API",
         "optional" : false,
         "projectId" : 306612,
         "recommendation" : "starred",
         "selected" : false,
         "serveronly" : false,
         "type" : "mod",
         "version" : "[1.16.4] Fabric API 0.28.3",
         "website" : "https://www.curseforge.com/minecraft/mc-mods/fabric-api"
      }
   },
   "version" : "1"
}
"""
##############################################################################
# THE END
