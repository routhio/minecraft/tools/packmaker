Configuration
=============

Packmaker has a simple configuration file that lets you contrl and customize its operation.
To configure packmaker, you create a file called ``packmaker.conf``. The location of this file
depends on your platform:

* On Linux and other Unix-like Oses, packmaker looks for ``~/.config/packmaker.conf``.
* On Windows, packmaker uses ``%APPDATA%\packmaker.conf``. This is usually a folder
  like ``C:\Users\You\AppData\Roaming``.
* On Mac OS X, you can use either the unix location or ``~/Library/Application Support/config.yml``

It possible to customize the location of the configuration file. See the *Configuration Location*,
below.

The config file uses a common INI syntax, with sections of the configuration delimited by
``[section]`` headers, and simple ``key = value`` parameters in each section.

You can include comments in the configuration file. Comments start with a ``#`` (hash), and
packmaker will ignore the comment until the end of the line.

You can also include environment variables in the values for most options, using the 
``${VARIABLE}`` syntax. Packmaker will attempt to resolve these definitions for the
environment.

The rest of this page enumerates all of the configuration options available.

Curseforge options
------------------
This section of the configuration controls how the Curseforge api is accessed and cached.

authentication_token
~~~~~~~~~~~~~~~~~~~~
The curseforge api authentication token.  This token is required to allow packmaker access
to the api.  Most api calls will fail without it, so packmaker will simply refuse to run
most commands if it is not defined.

You can obtain an authentication token with a curseforge.com_ account, using their
`Curseforge Console`_ site.

.. _curseforge.com: https://docs.curseforge.com/
.. _Curseforge Console: https://console.curseforge.com/#/api-keys

ignore_mods
~~~~~~~~~~~
A boolean value, when true tells packmaker to not include any mods when it is generating
the local moddb file, using the ``updatedb`` command.  This is an optimization if you know
you will not be dealing with mods in your modpack, and will speed up the ``updatedb`` operation.

This is optional, and defaults to ``false`` when not defined.  Mods will be included.

ignore_resourcepacks
~~~~~~~~~~~~~~~~~~~~
A boolean value, when true tells packmaker to not include any resourcepacks when it is generating
the local moddb file, using the ``updatedb`` command.  This is an optimization if you know
you will not be dealing with resourcepacks in your modpack, and will speed up the ``updatedb``
operation.

This is optional, and defaults to ``false`` when not defined.  Resourcepacks will be included.

moddb_filename
~~~~~~~~~~~~~~
The location and filename of the local cache of api information about mods and resourcepacks
from curseforge.  Packmaker uses this 'moddb' as a way to speed up many api calls, looking
up addon information from this file rather than calling curseforge. The ``updatedb`` command is
used to generate and maintain this file.

This file is optional. If you do not define it packmaker will still work, but it will be forced
to make many more curseforge api calls which can significantly slow down its operation.

HINT: If you specify a common location for this file, it can be reused for mulitple packs.
For example: ``moddb_filename = ${HOME}/packmaker/addons.db``.


Launch options
--------------
This section specifies parameters related to Java.  You can control specifically how java
is called when launching minecraft using the ``launch`` command, and when building minecraft
servers.

java_arguments
~~~~~~~~~~~~~~
The java command line arguments to use when launching Minecraft.  If you have unique
java requirements, you specify them here.

This is optional, and defaults to the following set of java arguments when not defined:

``-XX:+UseG1GC -XX:+UnlockExperimentalVMOptions``
``+XX:G1NewSizePercent=20 +XX:G1ReservePercent=20``
``-XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=32M``
``-Dsun.rmi.dgc.server.gcInterval=2147483646``
``-Dfml.readTimeout=180``

java_binary
~~~~~~~~~~~
The location of the actual Java executable to use when launching Minecraft. If java is not
on your ``PATH``, or you need to use the non-default version of java, you can specify it here.

This is optional, and defaults to just ``java`` when not defined.

java_memory
~~~~~~~~~~~
How much memory to assign to the java heap space when launching Minecraft. This game, especially
when modded, needs more memory to run than allocated by default by java itself.

This is optional, and default to ``4096 megabytes`` (``4 gigabytes``) when not defined.


Locations options
----------------
This section defines the locations, folders or directories, that packmaker will use when building
your modpack.

build
~~~~~
This is the root location of all build artifacts, although some can be changed using the location
configuratios below.

This is optional, and defaults to a directory named ``build`` in the current working directory.

Note that if not otherwise defined, both the cache and release locations are configured as subdirectories
of the build location.

cache
~~~~~
This location of files downloaded during the build command.  Minecraft files, Java libraries, mods and
resourcepacks are all downloaded to this cache location first, before being copied into their final
location for the modpack.

This is for optimization, to reduce multiple downloads of the sames file for subsequent builds. When
packmaker needs to download a file, it will first check this cache location, and use the cached file
rather than downloading another copy.  This greatly speeds up modpack building.

This is optional, and default to a directory named ``cache`` under the build location.

HINT: you can share this cache location between multiple modpacks if you defined it to be in a common
location. Then you will only ever download a version of minecraft once, and reuse it for every modpack.
For example: ``cache = ${HOME}/packmaker/cache``.

release
~~~~~~~
The location of the final release artifacts built by packmaker.  After a build command is completed you
will be able to find the zip file for your modpack in this location.

This is optional, and defaults to a directory named ``release`` under the build location.

Configuration Locations
-----------------------
Packmaker uses a specific search order to find its configuration file on the filesystem.  This search
order is dependent on the platform it is running on.

Linux or Unix-like systems
~~~~~~~~~~~~~~~~~~~~~~~~~~
On Linux and Unix, packmaker will look in the following locations for its configuration file, in this
order, using the first one it finds.

* ``./packmaker.conf``
* ``${XDG_CONFIG_HOME}/packmaker.conf``, if the XDG_CONFIG_HOME environment variable is defined
* ``~/.config/packmaker.conf``
* ``/usr/local/etc/packmaker.conf``
* ``/etc/packmaker.conf``

Windows
~~~~~~~
On Windows, packmaker will lock in the following locations for its configuration file, in this order,
using the first one it finds.

* ``.\packmaker.conf``
* ``~\AppData\Roaming\packmaker.conf``
* ``%APPDATA%\packmaker.conf``, if the APPDATA environment variable is defined

Mac OS X
~~~~~~~~
On Mac OS X, packmaker will look in the following locations for its configuration file, in this order,
using the first one it finds.

* ``./packmaker.conf``
* ``${XDG_CONFIG_HOME}/packmaker.conf``, if the XDG_CONFIG_HOME environment variable is defined
* ``~/.config/packmaker.conf``
* ``~/Library/Application Support/packmaker.conf``
* ``/usr/local/etc/packmaker.conf``
* ``/etc/packmaker.conf``


Example
-------
Here is an example configuration file, with all options defined::

    # packmaker.conf
    
    [curseforge]
    authentication_token = xxxxxxxxxxxxxxxxxxxxxxx
    ignore_mods = false
    ignore_resourcepacks = false
    moddb_filename = ${HOME}/.local/share/packmaker/curseforge.db
    
    [launch]
    java_arguments = -XX:+UseG1GC -XX:UseSSE=3
    java_binary = /usr/lib/jvm/java-8-openjdk-amd64/bin/java
    java_memory = 8192m
    
    [locations]
    build = ./build
    cache = ${HOME}/.local/share/packmaker/cache
    release = ./release
